import console.ConsoleBuffer;
import control.Controller;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by etheodor on 03/08/2016.
 */
public class ControllerTestSampleIO {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
    }

    @Test
    public void start() throws Exception {
        consoleBuffer = controller.processCommand("C 20 4", consoleBuffer);
        assertEquals(consoleBuffer.toString(), "----------------------\n" +
                "|                    |\n" +
                "|                    |\n" +
                "|                    |\n" +
                "|                    |\n" +
                "----------------------\n");

        consoleBuffer = controller.processCommand("L 1 2 6 2", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                "----------------------\n" +
                "|                    |\n" +
                "|XXXXXX              |\n" +
                "|                    |\n" +
                "|                    |\n" +
                "----------------------\n");

        consoleBuffer = controller.processCommand("L 6 3 6 4", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                "----------------------\n" +
                "|                    |\n" +
                "|XXXXXX              |\n" +
                "|     X              |\n" +
                "|     X              |\n" +
                "----------------------\n");

        consoleBuffer = controller.processCommand("R 16 1 20 3", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                "----------------------\n" +
                "|               XXXXX|\n" +
                "|XXXXXX         X   X|\n" +
                "|     X         XXXXX|\n" +
                "|     X              |\n" +
                "----------------------\n");

        consoleBuffer = controller.processCommand("B 10 3 o", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                "----------------------\n" +
                "|oooooooooooooooXXXXX|\n" +
                "|XXXXXXoooooooooX   X|\n" +
                "|     XoooooooooXXXXX|\n" +
                "|     Xoooooooooooooo|\n" +
                "----------------------\n");
    }

}