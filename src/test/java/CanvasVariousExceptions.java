import console.ConsoleBuffer;
import control.Controller;
import exceptions.InputException;
import org.junit.Before;
import org.junit.Test;


/**
 * Created by etheodor on 03/08/2016.
 */
public class CanvasVariousExceptions {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
    }

    @Test(expected = InputException.class)
    public void commandWithoutCanvas() throws Exception {
        consoleBuffer = controller.processCommand("R 5 5", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousCanvasCommand1() throws Exception {
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
        consoleBuffer = controller.processCommand("C 5 ", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousCanvasCommand2() throws Exception {
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
        consoleBuffer = controller.processCommand("CC 5 5", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousCanvasCommand3() throws Exception {
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
        consoleBuffer = controller.processCommand("C -1 5", consoleBuffer);
    }


}