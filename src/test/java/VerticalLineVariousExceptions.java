import console.ConsoleBuffer;
import control.Controller;
import exceptions.InputException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by etheodor on 03/08/2016.
 */
public class VerticalLineVariousExceptions {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousVerticalLineCommand1() throws Exception {
        consoleBuffer = controller.processCommand("L 2 2", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousVerticalLineCommand2() throws Exception {
        consoleBuffer = controller.processCommand("L 5 5 7", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousVerticalLineCommand3() throws Exception {
        consoleBuffer = controller.processCommand("L -2 5 4 5", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void outOfCanvasVerticalLineCommand1() throws Exception {
        consoleBuffer = controller.processCommand("L 12 5 12 18", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void outOfCanvasVerticalLineCommand2() throws Exception {
        consoleBuffer = controller.processCommand("L 5 12 5 18", consoleBuffer);
    }

    @Test
    public void partiallyInCanvasVerticalLineCommand() throws Exception {
        consoleBuffer = controller.processCommand("L 5 5 5 20", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                        "------------\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|    X     |\n" +
                        "|    X     |\n" +
                        "|    X     |\n" +
                        "|    X     |\n" +
                        "|    X     |\n" +
                        "|    X     |\n" +
                        "------------\n");
    }
}