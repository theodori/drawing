import console.ConsoleBuffer;
import control.Controller;
import exceptions.InputException;
import org.junit.Before;
import org.junit.Test;


/**
 * Created by etheodor on 03/08/2016.
 */
public class UnknownCommandsVariousExceptions {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
    }


    @Test(expected = InputException.class)
    public void unknownCommand1() throws Exception {
        consoleBuffer = controller.processCommand("TT 1 5", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void unknownCommand2() throws Exception {
        consoleBuffer = controller.processCommand(" 1 5", consoleBuffer);
    }
}