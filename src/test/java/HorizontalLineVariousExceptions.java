import console.ConsoleBuffer;
import control.Controller;
import exceptions.InputException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by etheodor on 03/08/2016.
 */
public class HorizontalLineVariousExceptions {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
    }


    @Test(expected = InputException.class)
    public void outOfCanvasHorizontalLineCommand1() throws Exception {
        consoleBuffer = controller.processCommand("L 12 12 18 12", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void outOfCanvasHorizontalLineCommand2() throws Exception {
        consoleBuffer = controller.processCommand("L 5 12 8 12", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void outOfCanvasHorizontalLineCommand3() throws Exception {
        consoleBuffer = controller.processCommand("L 12 5 18 5", consoleBuffer);
    }

    @Test
    public void partiallyInCanvasVerticalLineCommand() throws Exception {
        consoleBuffer = controller.processCommand("L 5 5 15 5", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                        "------------\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|    XXXXXX|\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "------------\n");
    }

    @Test(expected = InputException.class)
    public void notHorizontalOrVerticalLine() throws Exception {
        consoleBuffer = controller.processCommand("L 5 5 15 15", consoleBuffer);
    }
}