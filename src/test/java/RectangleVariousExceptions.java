import console.ConsoleBuffer;
import control.Controller;
import exceptions.InputException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by etheodor on 03/08/2016.
 */
public class RectangleVariousExceptions {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousRectangleCommand1() throws Exception {
        consoleBuffer = controller.processCommand("R 5 5", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousRectangleCommand2() throws Exception {
        consoleBuffer = controller.processCommand("R 5 5 7", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousRectangleCommand3() throws Exception {
        consoleBuffer = controller.processCommand("R -1 5", consoleBuffer);
    }


    @Test(expected = InputException.class)
    public void outOfCanvasRectangleCommand1() throws Exception {
        consoleBuffer = controller.processCommand("R 12 12 16 16", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void outOfCanvasRectangleCommand2() throws Exception {
        consoleBuffer = controller.processCommand("R 12 5 16 16", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void outOfCanvasRectangleCommand3() throws Exception {
        consoleBuffer = controller.processCommand("R 5 12 8 16", consoleBuffer);
    }


    @Test
    public void partiallyInCanvasRectangleCommand2() throws Exception {
        consoleBuffer = controller.processCommand("R 3 8 15 25", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                "------------\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|          |\n" +
                        "|  XXXXXXXX|\n" +
                        "|  X       |\n" +
                        "|  X       |\n" +
                        "------------\n");
    }

}