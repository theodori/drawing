import console.ConsoleBuffer;
import control.Controller;
import exceptions.DrawException;
import exceptions.InputException;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


/**
 * Created by etheodor on 03/08/2016.
 */
public class BucketFillVariousExceptions {
    Controller controller;
    ConsoleBuffer consoleBuffer;

    @Before
    public void setUp() throws Exception {
        controller = new Controller();
        consoleBuffer = controller.processCommand("C 10 10", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousCommand() throws Exception {
        consoleBuffer = controller.processCommand("B 5 5 ", consoleBuffer);
    }

    @Test(expected = InputException.class)
    public void erroneousColorCommand() throws Exception {
        consoleBuffer = controller.processCommand("B 5 5 X", consoleBuffer);
    }

    @Test(expected = DrawException.class)
    public void outOfCanvasCommand() throws Exception {
        consoleBuffer = controller.processCommand("B 15 15 o", consoleBuffer);
    }

    @Test
    public void bucketFill() throws Exception {
        consoleBuffer = controller.processCommand("B 10 10 T", consoleBuffer);
        assertEquals(consoleBuffer.toString(),
                        "------------\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "|TTTTTTTTTT|\n" +
                        "------------\n");
    }
}