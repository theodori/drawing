package exceptions;

/**
 * Exceptions on inappropriate user input commands
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class InputException extends Exception {
    public InputException(String message) {
        super(message);
    }
}
