package exceptions;

/**
 * Exceptions on drawing contents of canvas/console buffer
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class DrawException extends Exception {
    public DrawException(String message) {
        super(message);
    }
}
