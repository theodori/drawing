package console;

import control.Controller;
import exceptions.DrawException;
import exceptions.InputException;

import java.util.Scanner;

/**
 * Console class manages user input commands, passing commands to controller, handling exceptions of controller and
 * output exception messages to console. It maintains a buffer with the contents of canvas. On command execution,
 * it renders the contents of the buffer.
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class Console {
    private final Controller controller = new Controller();
    private ConsoleBuffer consoleBuffer;


    /**
     * Method that iteratively get input commands from user and pass them to controller for further processing.
     * On processing successfully a command then renders the content of canvas.
     * It handles InputExceptions that contain informative messages and output the messages to console.
     * General Exceptions should be only "logged"
     */
    public void start() {
        while (true) {
            String commandLine = getCommand();
            try {
                consoleBuffer = controller.processCommand(commandLine, consoleBuffer);
                renderConsoleBuffer();
            } catch (InputException e) {
                System.out.println(e.getMessage());
            } catch (DrawException e) {
                System.out.println(e.getMessage());
            } catch (Exception e) {
                System.out.println("Internal Error");
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to get input commands from console.
     *
     * @return The user input command as string
     */
    private String getCommand() {
        System.out.print("enter command:");
        Scanner in = new Scanner(System.in);

        String commandline = in.nextLine();
        while (commandline == null || commandline.length() == 0) {
            System.out.print("enter command:");
            commandline = in.nextLine();
        }
        return commandline;

    }

    /**
     * Method that renders canvas content to console
     */
    private void renderConsoleBuffer() {
        System.out.println(consoleBuffer.toString());
    }
}
