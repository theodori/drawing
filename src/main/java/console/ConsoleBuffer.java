package console;

import exceptions.DrawException;
import geometry.Canvas;

/**
 * ConsoleBuffer buffer maintains the contents of a canvas.
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class ConsoleBuffer {
    private int width;
    private int height;
    private Character[][] buffer;

    /**
     * ConsoleBuffer is initialized by a canvas object.
     * The content buffer adjusts to the proper size, content is cleared and margin of the canvas is set.
     *
     * @param canvas
     */
    public ConsoleBuffer(Canvas canvas) {
        this.width = canvas.getWidth() + 2;
        this.height = canvas.getHeight() + 2;
        buffer = new Character[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (i == 0 || i == height - 1) {
                    buffer[i][j] = '-';
                } else if (j == 0 || j == width - 1) {
                    buffer[i][j] = '|';
                } else {
                    buffer[i][j] = ' ';
                }
            }
        }
    }

    /**
     * Method that sets the content at a position of canvas.
     *
     * @param y column of the buffer.
     * @param x row of the buffer.
     * @param c color of the position.
     * @throws DrawException on writing outside the buffer.
     */
    public void setData(int y, int x, char c) throws DrawException {
        if ((x >= 1 && x < width - 1) && (y >= 1 || y < height - 1)) {
            buffer[y][x] = c;
        } else {
            throw new DrawException("Array Index out of bounds on ConsoleBuffer");
        }
    }

    /**
     * BucketFill method. Starts from position x,y of canvas and recursively set the color to adjacent positions.
     *
     * @param x column in canvas.
     * @param y row in canvas.
     * @param c color character (Cannot be |,-,X).
     */
    public void bucketFill(int x, int y, char c) throws DrawException {
        if (buffer[y][x] != '|' && buffer[y][x] != '-' && buffer[y][x] != 'X' && buffer[y][x] != c) {
            buffer[y][x] = c;
            bucketFill(x - 1, y - 1, c);
            bucketFill(x - 1, y, c);
            bucketFill(x - 1, y + 1, c);
            bucketFill(x + 1, y - 1, c);
            bucketFill(x + 1, y, c);
            bucketFill(x + 1, y + 1, c);
            bucketFill(x, y - 1, c);
            bucketFill(x, y + 1, c);
        }
    }

    /**
     * @return the width of canvas.
     */
    public int getWidth() {
        return width - 2;
    }

    /**
     * @return the height of canvas
     */
    public int getHeight() {
        return height - 2;
    }

    /**
     * @return The buffer/contents of canvas as a multi-line String.
     */
    @Override
    public String toString() {
        String bufferData = "";
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                bufferData += buffer[i][j];
            }
            bufferData += "\n";
        }
        return bufferData;
    }
}
