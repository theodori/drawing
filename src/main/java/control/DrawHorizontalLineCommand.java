package control;

import console.ConsoleBuffer;
import exceptions.DrawException;
import exceptions.InputException;
import geometry.HorizontalLine;

/**
 * DrawHorizontalLineCommand for drawing an Horizontal line
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class DrawHorizontalLineCommand extends HorizontalLine implements DrawGeometry {


    /**
     * @param y  Row in canvas
     * @param x1 Column in canvas - starting position of line
     * @param x2 Column in canvas - ending position of line
     */
    public DrawHorizontalLineCommand(int y, int x1, int x2) {
        super(y, x1, x2);
    }

    /**
     * Method that draws the horizontal line to the console buffer
     *
     * @param consoleBuffer console buffer/contents of canvas
     * @return updated console buffer/contents of canvas
     * @throws InputException on inappropriate canvas size
     * @throws DrawException  on drawing outside canvas
     */
    public ConsoleBuffer draw(ConsoleBuffer consoleBuffer) throws InputException, DrawException {
        if (consoleBuffer == null) {
            throw new InputException("Can not draw geometry on uninitialized canvas. Start a canvas C w h");
        }
        //check if horizontal line has an overlap with the canvas
        if ((this.getY() <= consoleBuffer.getHeight() && this.getX1() <= consoleBuffer.getWidth()) == false
                && (this.getY() <= consoleBuffer.getHeight() && this.getX2() <= consoleBuffer.getWidth()) == false) {
            //both ends of rectangle outside canvas
            throw new InputException("Vertical line Out of Canvas");
        }
        int row = this.getY();
        int x1 = Math.min(this.getX1(), this.getX2());
        int x2 = Math.max(this.getX1(), this.getX2());
        x1 = Math.max(1, x1);
        x2 = Math.min(x2, consoleBuffer.getWidth());
        for (int x = x1; x <= x2; x++) {
            consoleBuffer.setData(row, x, 'X');
        }
        return consoleBuffer;
    }


}
