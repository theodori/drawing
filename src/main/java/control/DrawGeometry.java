package control;

import console.ConsoleBuffer;
import exceptions.DrawException;
import exceptions.InputException;

/**
 * Created by etheodor on 03/08/2016.
 */
public interface DrawGeometry {

    ConsoleBuffer draw(ConsoleBuffer consoleBuffer) throws InputException, DrawException;

}
