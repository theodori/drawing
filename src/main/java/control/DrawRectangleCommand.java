package control;

import console.ConsoleBuffer;
import exceptions.DrawException;
import exceptions.InputException;
import geometry.Rectangle;

/**
 * DrawRectangleCommand for drawing a  Rectangle
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class DrawRectangleCommand extends Rectangle implements DrawGeometry {
    public DrawRectangleCommand(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
    }

    /**
     * Method that draws the rectangle line to the console buffer
     *
     * @param consoleBuffer console buffer/contents of canvas
     * @return updated console buffer/contents of canvas
     * @throws InputException on inappropriate canvas size
     * @throws DrawException  on drawing outside canvas
     */
    public ConsoleBuffer draw(ConsoleBuffer consoleBuffer) throws InputException, DrawException {
        if (consoleBuffer == null) {
            throw new InputException("Can not draw geometry on uninitialized canvas. Start a canvas C w h");
        }

        //check if rectangle has an overlap with the canvas
        if ((this.getX1() <= consoleBuffer.getWidth() && this.getY1() <= consoleBuffer.getHeight()) == false
                && (this.getX2() <= consoleBuffer.getWidth() && this.getY2() <= consoleBuffer.getHeight()) == false) {
            //both corners of rectangle outside canvas
            throw new InputException("Rectangle Out of Canvas");
        }


        // Draw rectangle as 2 horizontal and 2 vertical lines
        if (1 <= this.getY1() && this.getY1() <= consoleBuffer.getHeight()) {
            DrawHorizontalLineCommand drawHorizontalLine = new DrawHorizontalLineCommand(this.getY1(), this.getX1(), this.getX2());
            consoleBuffer = drawHorizontalLine.draw(consoleBuffer);
        }
        if (1 <= this.getY2() && this.getY2() <= consoleBuffer.getHeight()) {
            DrawHorizontalLineCommand drawHorizontalLine = new DrawHorizontalLineCommand(this.getY2(), this.getX1(), this.getX2());
            consoleBuffer = drawHorizontalLine.draw(consoleBuffer);
        }
        if (1 <= this.getX1() && this.getX1() <= consoleBuffer.getWidth()) {
            DrawVerticalLineCommand drawVerticalLine = new DrawVerticalLineCommand(this.getX1(), this.getY1(), this.getY2());
            consoleBuffer = drawVerticalLine.draw(consoleBuffer);
        }
        if (1 <= this.getX2() && this.getX2() <= consoleBuffer.getWidth()) {
            DrawVerticalLineCommand drawVerticalLine = new DrawVerticalLineCommand(this.getX2(), this.getY1(), this.getY2());
            consoleBuffer = drawVerticalLine.draw(consoleBuffer);
        }
        return consoleBuffer;
    }


}
