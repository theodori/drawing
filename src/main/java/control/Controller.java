package control;

import console.ConsoleBuffer;
import exceptions.DrawException;
import exceptions.InputException;

/**
 * Controller is processing user input commands. It validated each input-command and throws proper exceptions.
 * Based on the input-command created geometry draw commands and execute them. Geometry draw commands are updating
 * the console buffer than finally is rendered by console.
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class Controller {


    /**
     * Methods that
     *
     * @param commandLine   raw user input command as string
     * @param consoleBuffer buffer/contents of canvas
     * @return updated buffer/contents of canvas
     * @throws InputException on erroneous user input
     * @throws DrawException  on writing outside the canvas
     */
    public ConsoleBuffer processCommand(String commandLine, ConsoleBuffer consoleBuffer) throws InputException, DrawException {
        commandLine = commandLine.trim();
        if (!commandLine.matches("^[a-zA-Z0-9 ]*$")) { //check if input command contains non-alphanumeric chars. Trying to avoid !,. float input etc.
            throw new InputException("Invalid characters present in command.");
        }
        String[] parameters = commandLine.split(" "); //Splitting parts of command
        String c = parameters[0]; // First part should be the type of command
        if (c.equals("c") || c.equals("C")) { // Canvas Command
            if (parameters.length != 3) {
                throw new InputException("Invalid Number of Arguments: C w h, with w,h integers");
            }
            Integer width = Integer.valueOf(parameters[1]);
            Integer height = Integer.valueOf(parameters[2]);
            DrawCanvasCommand drawCanvas = new DrawCanvasCommand(width, height);
            consoleBuffer = drawCanvas.draw(consoleBuffer);
        } else if (c.equals("l") || c.equals("L")) { //Line command
            if (parameters.length != 5) {
                throw new InputException("Invalid Number of Arguments: L x1 y1 x2 y2, with x1,x2,y1,y2 integers");
            }
            Integer x1 = Integer.valueOf(parameters[1]);
            Integer y1 = Integer.valueOf(parameters[2]);
            Integer x2 = Integer.valueOf(parameters[3]);
            Integer y2 = Integer.valueOf(parameters[4]);
            if (x1 != x2 && y1 != y2) {
                throw new InputException("Not a Vertical/Horizontal Line");
            }
            if (y1 == y2) {
                DrawHorizontalLineCommand drawHorizontalLine = new DrawHorizontalLineCommand(y1, x1, x2);
                consoleBuffer = drawHorizontalLine.draw(consoleBuffer);
            } else {
                DrawVerticalLineCommand drawVerticalLine = new DrawVerticalLineCommand(x1, y1, y2);
                consoleBuffer = drawVerticalLine.draw(consoleBuffer);
            }
        } else if (c.equals("r") || c.equals("R")) { //Rectangle command
            if (parameters.length != 5) {
                throw new InputException("Invalid Number of Arguments: R x1 y1 x2 y2");
            }
            Integer x1 = Integer.valueOf(parameters[1]);
            Integer y1 = Integer.valueOf(parameters[2]);
            Integer x2 = Integer.valueOf(parameters[3]);
            Integer y2 = Integer.valueOf(parameters[4]);
            DrawRectangleCommand drawRectangle = new DrawRectangleCommand(x1, y1, x2, y2);
            consoleBuffer = drawRectangle.draw(consoleBuffer);
        } else if (c.equals("b") || c.equals("B")) { //BucketFill command
            if (parameters.length != 4) {
                throw new InputException("Invalid Number of Arguments: B x y c");
            }
            Integer x = Integer.valueOf(parameters[1]);
            Integer y = Integer.valueOf(parameters[2]);
            char color = parameters[3].charAt(0);
            if (color == 'X' || color == '-' || color == '|') {
                throw new InputException("Characters -,|,X can not be used as colors");
            }
            if (x>consoleBuffer.getWidth() || y>consoleBuffer.getHeight()){
                throw new DrawException("Bucket Fill start position outside canvas");
            }
            consoleBuffer.bucketFill(x, y, color);
        } else if (c.equals("q") || c.equals("Q")) { //Quit command
            System.out.println("Quit Application");
            System.exit(0);
        } else { //Command that starts with a character not mapper to a command
            throw new InputException("Unknown Command.Set of commands={C w h, L x1 y1 x2 y2, R x1 y1 x2 y2, Q}, where parameters w,h, x1,x2,y1,y2,x,y,c should be integers.");
        }
        return consoleBuffer;
    }
}
