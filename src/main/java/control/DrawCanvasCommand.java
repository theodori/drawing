package control;

import console.ConsoleBuffer;
import exceptions.InputException;
import geometry.Canvas;

/**
 * Command that draws a canvas. Re-initializes console buffer/contents of canvas on new canvas creation
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class DrawCanvasCommand extends Canvas implements DrawGeometry {
    public DrawCanvasCommand(int width, int height) {
        super(width, height);
    }

    /**
     * Method that draws the canvas to the console buffer
     *
     * @param consoleBuffer console buffer/contents of canvas
     * @return updated console buffer/contents of canvas
     * @throws InputException on inappropriate canvas size
     */
    public ConsoleBuffer draw(ConsoleBuffer consoleBuffer) throws InputException {
        if (this.getWidth() < 1 || this.getHeight() < 1) {
            throw new InputException("Not proper Canvas size. Minimum canvas is 1x1");
        }
        consoleBuffer = new ConsoleBuffer(this);
        return consoleBuffer;
    }


}
