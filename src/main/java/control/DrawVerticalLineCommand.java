package control;

import console.ConsoleBuffer;
import exceptions.DrawException;
import exceptions.InputException;
import geometry.VerticalLine;

/**
 * DrawHorizontalLineCommand for drawing an Horizontal line
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class DrawVerticalLineCommand extends VerticalLine implements DrawGeometry {

    /**
     * @param x  Column in canvas
     * @param y1 Row in canvas - starting position of line
     * @param y2 row in canvas - ending position of line
     */
    public DrawVerticalLineCommand(int x, int y1, int y2) {
        super(x, y1, y2);
    }

    /**
     * Method that draws the vertical line to the console buffer
     *
     * @param consoleBuffer console buffer/contents of canvas
     * @return updated console buffer/contents of canvas
     * @throws InputException on inappropriate canvas size
     * @throws DrawException  on drawing outside canvas
     */
    public ConsoleBuffer draw(ConsoleBuffer consoleBuffer) throws InputException, DrawException {
        if (consoleBuffer == null) {
            throw new InputException("Can not draw geometry on uninitialized canvas. Start a canvas C w h");
        }


        //check if vertical line has an overlap with the canvas
        if ((this.getX() <= consoleBuffer.getWidth() && this.getY1() <= consoleBuffer.getHeight()) == false
                && (this.getX() <= consoleBuffer.getWidth() && this.getY2() <= consoleBuffer.getHeight()) == false) {
            //both ends of rectangle outside canvas
            throw new InputException("Vertical line Out of Canvas");
        }

        int column = this.getX();
        int y1 = Math.min(this.getY1(), this.getY2());
        int y2 = Math.max(this.getY1(), this.getY2());
        y1 = Math.max(1, y1);
        y2 = Math.min(y2, consoleBuffer.getHeight());
        for (int y = y1; y <= y2; y++) {
            consoleBuffer.setData(y, column, 'X');
        }
        return consoleBuffer;
    }


}
