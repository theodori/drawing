import console.Console;

/**
 * Application thas starts the program
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class Application {

    public static void main(String[] args) {
        Console console = new Console();
        console.start();
    }
}
