package geometry;

/**
 * Vertical Line Model
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class VerticalLine {
    private int x;
    private int y1;
    private int y2;

    public VerticalLine(int x, int y1, int y2) {
        this.x = x;
        this.y1 = y1;
        this.y2 = y2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    @Override
    public String toString() {
        return "VerticalLine{" +
                "x=" + x +
                ", y1=" + y1 +
                ", y2=" + y2 +
                '}';
    }
}
