package geometry;

/**
 * Horizontal Line Model
 * <p>
 * Created by etheodor on 03/08/2016.
 */
public class HorizontalLine {
    private int y;
    private int x1;
    private int x2;

    public HorizontalLine(int y, int x1, int x2) {
        this.y = y;
        this.x1 = x1;
        this.x2 = x2;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }


    @Override
    public String toString() {
        return "HorizontalLine{" +
                "y=" + y +
                ", x1=" + x1 +
                ", x2=" + x2 +
                '}';
    }
}
