Simple Console Drawing Program
====================

# Description of functionalities 

This is a simple console version of a drawing program. The functionality of the
program is quite limited. The program works as follows:

* create a new canvas.
* start drawing on the canvas by issuing various commands.
* quit.

The program is supporting  commands:

C w h 
>Creates a new canvas of width w and height h.


L x1 y1 x2 y2 
>Creates a new line from (x1,y1) to (x2,y2). Currently only horizontal or
vertical lines are supported. Horizontal and vertical lines will be drawn using the 'x'
character.


R x1 y1 x2 y2 
> Creates a new rectangle, whose upper left corner is (x1,y1) and lower right
corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.

B x y c 
> Fills the entire area connected to (x,y) with colour 'c'. The behaviour of this is
the same as that of the "bucket fill" tool in paint programs.

Q 
> Quits the program.

# Various Assumptions 
* User input commands should contain only alphanumeric characters
* Drawing lines that are completely outside the canvas is producing exceptions
* Drawing lines that are partially inside the canvas then the intersection of line with the canvas is rendered
* Drawing rectangles that are completely outside the canvas is producing exceptions
* Drawing rectangles that are partially inside the canvas then the intersection of rectangle with the canvas is rendered
* Bucket fill in order to work the starting point should be on a not line, rectangle position 
 
# Building and Executing 

Building and executing the the project can be managed with gradle (https://gradle.org/). It is not required as there not
fetched dependencies from an online MVN repo. Run commands on top project directory (use gradlew.bat on windows console).
 
 gradle build
> Build the project (including running the unit tests)
 
 java -jar .\build\libs\drawing-1.0-SNAPSHOT.jar
>  Execute the produced jar/program
 
 gradle test
>  Execute unit tests
  
  
  A working build can found here [Jar](https://bitbucket.org/theodori/drawing/raw/658ff62dcac703d24db54404673aadda79a57e36/dist/drawing-1.0-SNAPSHOT.jar)
 